# Example Setup Webpack + React
## 1. setup webpack
`yarn add webpack webpack-cli --dev`

ในไฟล์ package.json เพิ่มสคริปสำหรับ build

```json
"scripts": {
  "build": "webpack"
}
```

เพิ่ม Config ด้วย webpack.config.js  
`yarn add path --dev`

```javascript
const { resolve } = require('path')
module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'bundle.js',
    path: resolve(__dirname, 'dist'),
  },
}
```

## 2. setup react
`yarn add react react-dom`

เพิ่มโค้ดใน src/index.js ซึ่งเป็น entry point ของเว็บ

```javascript
import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
ReactDOM.render(<App />, document.getElementById('root'))
```

แล้วก็สร้างไฟล์ src/App.js เพื่อสร้าง component App ขึ้นมา

```javascript
import React from 'react'
const App = () => <h1>Webpack React Setup.</h1>
export default App
```

## 3. setup webpack loader
`yarn add babel-core babel-loader@7 babel-preset-env babel-preset-react --dev`

และเพิ่ม config ของ loader ใน webpack.config.js

```javascript
module.exports = {
  ...
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['babel-preset-env', 'babel-preset-react'],
          },
        },
      },
    ],
  },
}
```

**option**
**test: /\.js$/** — เราต้องกำหนดก่อนว่าจะให้ไฟล์ไหนใช้ loader ได้บ้าง โดยกำหนดเป็น regex  
**exclude: /(node_modules)/** — เราสามารถเลือก exclude ไฟล์ที่ไม่ต้องการแปลงได้ครับ (กลับกันสามารถเลือกเฉพาะไฟล์ที่เราอยากแปลงได้โดยเปลี่ยน key จาก exclude เป็น include)  
**loader: 'babel-loader'** — เป็นชื่อของ loader ที่เราจะใช้ ในที่นี่คือ babel-loader เอาไว้ transpile javascript  
**options** — เราสามารถเพิ่ม options ให้กับแต่ละ loader ได้ จากตัวอย่างเป็นการเลือก presets ในการแปลงครับ ซึ่ง babel-preset-env เอาไว้แปลง ES6 และ babel-preset-react ไว้แปลง JSX  

## 4. auto generate html (ในการสร้างไฟล์ index.html กัน โดยทุกครั้งที่ build จะได้ไฟล์ dist/index.html ขึ้นมา)
`yarn add html-webpack-plugin --dev`

แล้วเพิ่มโค้ดใน webpack.config.js ดังนี้

```javascript
const HtmlWebpackPlugin = require('html-webpack-plugin')
module.exports = {
  ...
  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/index.html',
    }),
  ],
}
```

```javascript
<!DOCTYPE html>
<html>
  <head>
    <title>Webpack + React Setup</title>
  </head>
  <body>
    <div id="root"></div>
  </body>
</html>
```

## 5. setup Webpack Dev Server
`yarn add webpack-dev-server --dev`

เพิ่ม script dev ใน package.json

```json
"scripts": { 
  "dev": "webpack-dev-server --open",
  "build": "webpack"  
}
```

ใส่ mode เพื่อความเร็วในการ build

```json
"scripts": {
  "dev": "webpack-dev-server --mode development --open",
  "build": "webpack --mode production"
}
```

## 6. setup loader css and scss
**css**  
`yarn add css-loader style-loader --dev`

เพิ่ม loader ใน webpack.config.js

```javascript
module.exports = {
  ...
  module: {
    rules: [
      {
        ...
      },
      {
        test: /\.css$/,
        exclude: /(node_modules)/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
        ],
      },
    ],
  },
}
```

**scss**  
`yarn add sass-loader node-sass --dev`

แล้วเพิ่ม loader สำหรับไฟล์ scss

```javascript
module.exports = {
  ...
  module: {
    rules: [
      {
        ...
      },
      {
        test: /\.scss$/,
        exclude: /(node_modules)/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          { loader: 'sass-loader' },
        ],
      },
    ],
  },
}
```

## 7. setup file loader
`yarn add file-loader --dev`

เพิ่ม loader ในไฟล์ webpack.config.js

```javascript
module.exports = {
  ...
  module: {
    rules: [
      {
        ...
      },
      {
        test: /\.png$/,
        exclude: /(node_modules)/,
        use: [
          { loader: 'file-loader' },
        ],
      },
    ],
  },
}
```

เพิ่มโค้ด import รูปภาพใน src/App.js

```javascript
import React from 'react'
import logo from '../assets/images/logo.png'
import './App.css'
const App = () => (
  <div>
    <img src={logo} />
    <h1>Webpack React Setup.</h1>
  </div>
)
export default App
```

## 8. setup Hot Module Replacement (no refresh)
`yarn add react-hot-loader --dev`

เพิ่มโค้ดใน webpack.config.js

```javascript
const webpack = require('webpack')
module.exports = {
  ...
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['babel-preset-env', 'babel-preset-react'],
            plugins: ["react-hot-loader/babel"],
          },
        },
      },
    ],
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      template: 'src/index.html',
    }),
  ],
  devServer: {
    hot: true,
  },
}
```

แล้วแก้ src/App.js ครับ

```javascript
import React from 'react'
import { hot } from 'react-hot-loader'
import logo from './assets/images/logo.png'
import './App.css'
const App = () => (
  <div>
    <img src={logo} />
    <h1>Webpack React Setup.</h1>
  </div>
)
export default hot(module)(App)
```

## 9. setup clean webpack
`yarn add clean-webpack-plugin --dev`

เพิ่ม plugin ใน webpack.config.js

```javascript
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
module.exports = {
  ...
  plugins: [
    new CleanWebpackPlugin(['dist']),
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      template: 'src/index.html',
    }),
  ],
}
```